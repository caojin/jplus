package io.jplus;

import io.jplus.admin.model.Menu;
import io.jplus.utils.JplusJson;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Retire 吴益峰 （372310383@qq.com）
 * @version V1.0
 * @Title:
 * @Package io.jplus
 * @create 2019-01-08 16:36
 */
public class JsonTest {


    @Test
    public void testToJson() {

        Menu menu = new Menu();
        menu.put("id", 1);
        List subList = new ArrayList<>();
        Menu subMenu = new Menu();
        subMenu.put("id", "2");
        subMenu.put("name", "test");

        subList.add(subMenu);
        menu.setList(subList);

        //依赖model attr
        JplusJson jbootJson = new JplusJson();

        //依赖get set 方法
        //String jsonStr =FastJson.getJson().toJson(menu);

        String jsonStr = jbootJson.toJson(menu);
        System.out.println(jsonStr);
    }

}
