package io.jplus.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.JplusConsts;
import io.jplus.admin.model.Oss;
import io.jplus.admin.service.OssService;
import io.jplus.core.web.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping(value = "/admin/oss", viewPath = JplusConsts.BASE_VIEW_PATH + "admin")
public class OssController extends BaseController {

    @RPCInject
    OssService ossService;

    public void index() {
        render("oss.html");
    }


    @RequiresPermissions("sys:oss:list")
    public void list() {
        Page<Oss> page = ossService.queryPage(getQuery());
        renderJsonPage(page);
    }

    @RequiresPermissions("sys:oss:info")
    public void info() {

    }

    @Before(POST.class)
    @RequiresPermissions("sys:oss:save")
    public void save() {

    }

    @Before(POST.class)
    @RequiresPermissions("sys:oss:update")
    public void update() {

    }

    @RequiresPermissions("sys:oss:delete")
    public void delete() {

    }

}
