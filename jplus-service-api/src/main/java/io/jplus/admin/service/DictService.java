package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.Dict;
import io.jplus.common.Query;

import java.util.List;

public interface DictService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Dict findById(Object id);


    /**
     * find all model
     *
     * @return all <Dict
     */
    public List<Dict> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Dict model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(Dict model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(Dict model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Dict model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<Dict> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<Dict> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<Dict> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    Page<Dict> queryPage(Query query);

    public boolean copy(Object id);

    public boolean deleteById(Object... ids);
}